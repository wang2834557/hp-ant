
项目下载和运行
----

```
- npm安装淘宝镜像
```
npm install -g cnpm --registry=https://registry.npm.taobao.org
```
- npm 安装yarn
```
npm install -g yarn
```

- 安装依赖
```
yarn install
```

- 开发模式运行
```
yarn run serve
```

- 编译项目
```
yarn run build
```

- Lints and fixes files
```
yarn run lint
```
